package com.alex.mobilerefresh;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {



    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.alex.mobilerefresh", appContext.getPackageName());
    }


    @Test
    public void assertThatAllItem_AreInVisible_Click_Login(){
        MainActivity activity = spy(mActivityTestRule.getActivity());

        onView(ViewMatchers.withId(R.id.et_password)).check(matches(isDisplayed())).perform(replaceText("test"));
        onView(ViewMatchers.withId(R.id.et_username)).check(matches(isDisplayed()))
                .perform(replaceText("test"))
        .check(matches(withText("test")));
        onView(ViewMatchers.withId(R.id.bt_login)).check(matches(isDisplayed()))
        .perform(click());
        
    }


    @Test
    public void testSave_SharedPreferencesData(){


    }


}
