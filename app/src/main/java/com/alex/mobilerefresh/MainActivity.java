package com.alex.mobilerefresh;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.et_username)
    TextView etUsername;

    @BindView(R.id.et_password)
    TextView etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init Butter Knife
        ButterKnife.bind(this);
    }



    @OnClick(R.id.bt_login)
    void loginAction(){
        Log.d(this.getComponentName().toString(),  "Click no Butão de login");
        testClick();

    }

    public void testClick(){
        Log.d(this.getComponentName().toString(),  "Click test method");
    }
}
