package com.alex.mobilerefresh;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;

/**
 * Created by nb19875 on 19/06/2017.
 */

public class MyApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
        // Init stetho
        Stetho.initializeWithDefaults(this);
    }

    public static Context getContext() {
        return context;
    }
}
