package com.alex.mobilerefresh;

import com.alex.mobilerefresh.network.RestClient;
import com.alex.mobilerefresh.network.ServiceApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.CountDownLatch;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nb19875 on 19/06/2017.
 */


@RunWith(MockitoJUnitRunner.class)
public class HttpMockServerTest {

    private static final String LOGIN = "src/test/assets/login/svcResponseOk";

    private CountDownLatch lock = new CountDownLatch(1);
    MockWebServer mockWebServer;
    Retrofit retrofit;
    ServiceApi api;

    @Mock
    RestClient restClient;

    @Before
    public void setup(){

        MockitoAnnotations.initMocks(this);

        mockWebServer = new MockWebServer();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

         retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
         api = retrofit.create(ServiceApi.class);

        RestClient.BASE_URL ="http://localhost:"+mockWebServer.getPort();
    }


    @Test
    public void loginIntegration_requestSuccess() throws Exception {
        // set response
        mockWebServer.enqueue(new MockResponse().setBody(getStringFromFile(getFileFromloc(LOGIN))));


        // lock.await(500, TimeUnit.MILLISECONDS);
    }




    @After
    public void tearDown() throws IOException {
        mockWebServer.shutdown();
    }




    // Utils


    public static File getFileFromloc(String loc){

        return new File("src/test/assets/login/svcResponseOk");
    }

    public static String getStringFromFile(File file) throws Exception {
        final InputStream stream = new FileInputStream(file);

        String ret = convertStreamToString(stream);
        //Make sure you close all streams.
        stream.close();
        return ret;
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

}
